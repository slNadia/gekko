<?php

use yii\db\Migration;

class m171031_092556_create_Files_table extends Migration
{
	public function safeUp()
	{
		$this->createTable('Files', [
			'id'            => $this->primaryKey(),
			'filePath'      => $this->string()->notNull(),
			'originalTitle' => $this->string()->notNull(),
			'title'         => $this->string()->notNull(),
			'dateAdded'     => $this->integer()->notNull()
		]);
	}
	
	public function safeDown()
	{
		$this->dropTable('Files');
	}
}
