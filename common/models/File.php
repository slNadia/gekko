<?php

namespace common\models;

use Sunra\PhpSimple\HtmlDomParser;
use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "Files".
 *
 * @property integer $id
 * @property string  $filePath
 * @property string  $originalTitle
 * @property string  $title
 * @property integer $dateAdded
 */
class File extends ActiveRecord
{
	public $file;
	public static $path = 'files/';
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'Files';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['filePath', 'originalTitle', 'title', 'dateAdded', 'file'], 'required'],
			[['dateAdded'], 'integer'],
			[['filePath', 'originalTitle', 'title'], 'string', 'max' => 255],
			['file', 'file', 'extensions' => 'html', 'maxFiles' => 1, 'maxSize' => 1024000,
			                 'tooBig'     => 'Размер файла не должен превышать 1МБ']
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'            => 'ID',
			'filePath'      => 'Путь к файлу',
			'originalTitle' => 'Оригинальное название',
			'title'         => 'Название',
			'dateAdded'     => 'Дата добавления',
			'file'          => 'Файл'
		];
	}
	
	/**
	 * Загрузка файла
	 *
	 * @return bool
	 */
	public function addFile()
	{
		$file = UploadedFile::getInstance($this, 'file');
		if ($file) {
			$fileTitle = round(microtime(true) * 1000) . '.' . $file->extension;
			$file->saveAs(self::$path . $fileTitle);
			$check = $this->errorInFile($fileTitle);
			if ($check) {
				unlink(self::$path . $fileTitle);
				
				return 'Некорректное содержимое файла: ' . $check . '. Исправьте ошибки и попробуйте загрузить файл повторно.';
			}
			
			$this->filePath = self::$path . $fileTitle;
			$this->originalTitle = $file->name;
			$this->title = $fileTitle;
			$this->dateAdded = time();
			if ($this->save(false)) {
				return true;
			}
		}
		
		return 'Возникла ошибка при загрузке файла';
	}
	
	/**
	 * Проверка на корректность файла
	 * В файле должна быть таблица "Closed Transactions" и в ней хоть одна строчка с ценой
	 *
	 * @param $fileTitle
	 *
	 * @return bool
	 */
	public static function errorInFile($fileTitle)
	{
		$html = HtmlDomParser::file_get_html(self::$path . $fileTitle, false, null, null);
		if (!$html->find('table')) {
			return 'Не найдено ни одной таблицы';
		}
		$isClosedTransaction = false;
		$isEmptyTable = true;
		foreach ($html->find('tr') as $element) {
			if ($element->first_child()->plaintext == 'Closed Transactions:') {
				$isClosedTransaction = true;
			} elseif ($isClosedTransaction && $element->first_child()->find('b')) {
				break;
			}
			if ($isClosedTransaction) {
				if ($element->first_child()->plaintext != '&nbsp;') {
					$price = str_replace(' ', '', $element->last_child()->plaintext);
					if ($element->find('.mspt') && is_numeric($price)) {
						$isEmptyTable = false;
						break;
					}
				}
			}
		}
		if (!$isClosedTransaction) {
			return 'Нет таблицы "Closed Transactions"';
		}
		if ($isEmptyTable) {
			return 'Таблица "Closed Transactions" пуста';
		}
		
		return false;
	}
	
	/**
	 * Парсинг html файла
	 *
	 * @return array
	 */
	public function parsing()
	{
		$balance = 0; //начальный баланс
		$html = HtmlDomParser::file_get_html($this->filePath, false, null, null); //содержимое html-файла
		$profit = []; //прибыль
		$tickets = []; //даты транзакций
		$isClosedTransaction = false;
		//Прибыль лежит в таблице в строках, для которых выполняются определенные условия:
		foreach ($html->find('tr') as $element) {
			//Берём только закрытые транзакции
			if ($element->first_child()->plaintext == 'Closed Transactions:') {
				$isClosedTransaction = true;
			} elseif ($isClosedTransaction && $element->first_child()->find('b')) {
				break;
			}
			if ($isClosedTransaction) {
				//у колонки есть класс mspt
				if ($element->first_child()->plaintext != '&nbsp;') {
					$price = str_replace(' ', '', $element->last_child()->plaintext);
					//и в колонке лежит число (is_numeric отсечёт значения типа cancelled)
					if ($element->find('.mspt') && is_numeric($price)) {
						$balance += (float)$price;
						$profit[] = round($balance, 2);
						$tickets[] = $element->first_child()->plaintext;
					}
				}
			}
		}
		$html->clear();
		unset($html);
		
		return ['profit' => $profit, 'tickets' => $tickets];
	}
	
	/**
	 * @param null $notExist
	 *
	 * @return $this[] | null
	 */
	public static function getAll($notExist = null)
	{
		$result = self::find();
		if ($notExist) {
			$result->andWhere(['not', ['id' => $notExist]]);
		}
		
		return $result->orderBy(['dateAdded' => SORT_ASC])->all();
	}
}
