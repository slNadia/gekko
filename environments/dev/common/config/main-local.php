<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=developer',
            'username' => 'postgres',
            'password' => 'oogeec4cai',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
    'aliases' => [
        '@files' => 'C:\xampp\htdocs\developer174\files',
        '@filesView' => 'http://files.developer174.ru/'
    ]
];
