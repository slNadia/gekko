<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=gekko',
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ]
    ],
];
