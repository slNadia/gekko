<?php

use miloschuman\highcharts\Highcharts;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var \yii\web\View              $this
 * @var \common\models\File        $model
 * @var \common\models\File | null $fileForParsing
 * @var array                      $data
 * @var string | null              $error
 * @var string | null              $fileError
 */

$this->title = 'Парсинг файла';
?>
    <div class="file-form">
		<?php if ($error) { ?>
            <div class="alert alert-danger"><?= $error ?></div>
		<?php } ?>
		<?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-sm-6">
				<?= $form->field($model, 'file')->fileInput() ?>
            </div>
            <div class="col-sm-6">
				<?= Html::submitButton('Загрузить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
		<?php $form->end(); ?>
    </div>

    <div class="alert alert-info">
        По умолчанию открывается последний загруженный файл
		<?php if ($files = \common\models\File::getAll($fileForParsing ? $fileForParsing->id : null)) { ?>
            <br>
            <div class="btn-group">
                <button type="button"
                        class="btn btn-default">Выберите другой файл
                </button>
                <button type="button" class="btn btn-default dropdown-toggle"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
					<?php
					foreach ($files as $file) {
						?>
                        <li>
                            <a href="<?= \yii\helpers\Url::to(['index', 'id' => $file->id]) ?>">
								<?= $file->originalTitle ?> (<?= date("d.m.y, H:i", $file->dateAdded) ?>)
                            </a>
                        </li>
						<?php
					}
					?>
                </ul>
            </div>
		<?php } ?>
        <br>
		<?php if ($fileForParsing) { ?>
			<?= Html::a('Нажмите, чтобы посмотреть таблицу', ['table', 'id' => $fileForParsing->id], ['target' => '_blank']) ?>
		<?php } ?>
    </div>

<?php if ($data) { ?>
    <div class="graphic">
		<?= Highcharts::widget([
			'options' => [
				'title'  => ['text' => \Yii::t('app', 'График прибыли')],
				'xAxis'  => [
					'categories' => $data['tickets']
				],
				'yAxis'  => [
					'title' => ['text' => 'Прибыль']
				],
				'series' => [
					['name' => 'Изменение прибыли', 'data' => $data['profit']]
				]
			]
		]); ?>
    </div>
<?php } elseif ($fileError) { ?>
    <div class="alert alert-danger">
        Некорректный файл: <?= $fileError ?>. Построение графика невозможно.
    </div>
<?php } ?>