<?php
namespace admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class BaseController extends Controller
{
	
	public function can($role)
	{
		if (!\Yii::$app->user->can($role)) {
			throw new ForbiddenHttpException('Доступ запрещён');
		}
		return true;
	}
}
