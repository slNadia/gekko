<?php

namespace admin\controllers;

use common\models\File;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends BaseController
{
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			]
		];
	}
	
	public function actionIndex($id = null)
	{
		// Загрузка файла
		$model = new File();
		$error = null;
		if ($model->load(\Yii::$app->request->post())) {
			$result = $model->addFile();
			if ($result === true) {
				return $this->redirect(['index']);
			} else {
				$error = $result;
			}
		}
		
		/** @var File $fileForParsing Файл, который будет отображаться при заходе на страницу */
		$fileForParsing = $id ? File::findOne($id) : File::find()->orderBy(['dateAdded' => SORT_DESC])->one();
		$data = [];
		$fileError = null;
		if ($fileForParsing) {
			if (!$fileError = $fileForParsing->errorInFile($fileForParsing->title)) {
				$data = $fileForParsing->parsing();
			}
		}
		
		return $this->render('index', [
			'model'          => $model,
			'fileForParsing' => $fileForParsing,
			'data'           => $data,
			'error'          => $error,
			'fileError'      => $fileError
		]);
	}
	
	public function actionTable($id)
	{
		$fileModel = File::findOne($id);
		if (!$fileModel) {
			throw new NotFoundHttpException('File not found');
		}
		
		$content = file_get_contents($fileModel->filePath);
		
		return $content;
	}
}
